package com.dg.mall.cache.common.constant;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: wuwen
 * @createTime: 2021-07-04 11:44
 **/

public class ElasticSearchConstant {
	
	
	//--------------------DTS消息日志记录相关的----------------------
	public static final String DTS_MESSAGE_LOG_INDEXNAME="dts_message_log";
		
	public static final String DTS_MESSAGE_LOG__INDEXTYPE="dts_message_log_type";
		
	public static final String DTS_MESSAGE_LOG_ALIASES="dts_message_log_aliases";
	
	
	
	

	//--------------------商品相关的----------------------
	public static final String PRODUCT_INDEXNAME="product_v1";
	
	public static final String PRODUCT_INDEXTYPE="product_type";
	
	public static final String PRODUCT_ALIASES="product_aliases";
	
	
	
	
	
	
	
	
	
}
