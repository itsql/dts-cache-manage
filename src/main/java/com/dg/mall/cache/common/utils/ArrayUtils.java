package com.dg.mall.cache.common.utils;

import java.util.ArrayList;
import java.util.List;

public class ArrayUtils extends org.apache.commons.lang3.ArrayUtils {
	
	//大数组拆分多个小数组
	public static <T> List<List<T>> spliceArrays(List<T> list, int splitSize) {
	     if(list == null) {
	         return null;
	     }
	     if(splitSize<1) {
	    	 splitSize=1;
	     }
	     List<List<T>> rows =  new  ArrayList<>();
	     int totalSize = list.size();
	     int count = (totalSize % splitSize ==  0 ) ?
	             (totalSize / splitSize) : (totalSize/splitSize+ 1 );
	     for(int  i =  0 ; i < count;i++) {
	         List<T> cols = list.subList(i * splitSize,
	                 (i == count -  1 ) ? totalSize : splitSize * (i +  1 ));
	         rows.add(cols);
	     }
	     return  rows;
	}

	
	
	
}
