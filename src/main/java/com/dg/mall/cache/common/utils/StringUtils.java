package com.dg.mall.cache.common.utils;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringEscapeUtils;

import com.google.common.collect.Lists;


@SuppressWarnings("deprecation")
public class StringUtils extends org.apache.commons.lang3.StringUtils {

    private static final char SEPARATOR = '_';
    private static final String UNDERLINE = "_";

    private static final String CHARSET_NAME = "UTF-8";

    /**
     * 转换为字节数组
     *
     * @param str
     * @return
     */
    public static byte[] getBytes(String str) {
        if (str != null) {
            try {
                return str.getBytes(CHARSET_NAME);
            } catch (UnsupportedEncodingException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 字节数组转换为String
     *
     * @param bytes
     * @return
     */
    public static String byteToString(byte[] bytes) {
        try {
            return new String(bytes, CHARSET_NAME);
        } catch (UnsupportedEncodingException e) {
            return EMPTY;
        }
    }

    /**
     * 是否包含字符串
     *
     * @param str  验证字符串
     * @param strs 字符串组
     * @return 包含返回true
     */
    public static boolean inString(String str, String... strs) {
        if (str != null) {
            for (String s : strs) {
                if (str.equals(trim(s))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 替换掉HTML标签方法
     */
    public static String replaceHtml(String html) {
        if (isBlank(html)) {
            return "";
        }
        String regEx = "<.+?>";
        Pattern p = compile(regEx);
        Matcher m = p.matcher(html);
        String s = m.replaceAll("");
        return s;
    }

    /**
     * 替换为手机识别的HTML，去掉样式及属性，保留回车。
     *
     * @param html
     * @return
     */
    public static String replaceMobileHtml(String html) {
        if (html == null) {
            return "";
        }
        return html.replaceAll("<([a-z]+?)\\s+?.*?>", "<$1>");
    }

    /**
     * 替换为手机识别的HTML，去掉样式及属性，保留回车。
     *
     * @param txt
     * @return
     */
    public static String toHtml(String txt) {
        if (txt == null) {
            return "";
        }
        return replace(replace(Encodes.escapeHtml(txt), "\n", "<br/>"), "\t", "&nbsp; &nbsp; ");
    }

    /**
     * 缩略字符串（不区分中英文字符）
     *
     * @param str    目标字符串
     * @param length 截取长度
     * @return
     */
    public static String abbr(String str, int length) {
        if (str == null) {
            return "";
        }
        try {
            StringBuilder sb = new StringBuilder();
            int currentLength = 0;
            for (char c : replaceHtml(StringEscapeUtils.unescapeHtml4(str)).toCharArray()) {
                currentLength += String.valueOf(c).getBytes("GBK").length;
                if (currentLength <= length - 3) {
                    sb.append(c);
                } else {
                    sb.append("...");
                    break;
                }
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String abbr2(String param, int length) {
        if (param == null) {
            return "";
        }
        StringBuffer result = new StringBuffer();
        int n = 0;
        char temp;
        boolean isCode = false; // 是不是HTML代码
        boolean isHTML = false; // 是不是HTML特殊字符,如&nbsp;
        for (int i = 0; i < param.length(); i++) {
            temp = param.charAt(i);
            if (temp == '<') {
                isCode = true;
            } else if (temp == '&') {
                isHTML = true;
            } else if (temp == '>' && isCode) {
                n = n - 1;
                isCode = false;
            } else if (temp == ';' && isHTML) {
                isHTML = false;
            }
            try {
                if (!isCode && !isHTML) {
                    n += String.valueOf(temp).getBytes("GBK").length;
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if (n <= length - 3) {
                result.append(temp);
            } else {
                result.append("...");
                break;
            }
        }
        // 取出截取字符串中的HTML标记
        String temp_result = result.toString().replaceAll("(>)[^<>]*(<?)",
                "$1$2");
        // 去掉不需要结素标记的HTML标记
        temp_result = temp_result
                .replaceAll(
                        "</?(AREA|BASE|BASEFONT|BODY|BR|COL|COLGROUP|DD|DT|FRAME|HEAD|HR|HTML|IMG|INPUT|ISINDEX|LI|LINK|META|OPTION|P|PARAM|TBODY|TD|TFOOT|TH|THEAD|TR|area|base|basefont|body|br|col|colgroup|dd|dt|frame|head|hr|html|img|input|isindex|li|link|meta|option|p|param|tbody|td|tfoot|th|thead|tr)[^<>]*/?>",
                        "");
        // 去掉成对的HTML标记
        temp_result = temp_result.replaceAll("<([a-zA-Z]+)[^<>]*>(.*?)</\\1>",
                "$2");
        // 用正则表达式取出标记
        Pattern p = compile("<([a-zA-Z]+)[^<>]*>");
        Matcher m = p.matcher(temp_result);
        List<String> endHTML = Lists.newArrayList();
        while (m.find()) {
            endHTML.add(m.group(1));
        }
        // 补全不成对的HTML标记
        for (int i = endHTML.size() - 1; i >= 0; i--) {
            result.append("</");
            result.append(endHTML.get(i));
            result.append(">");
        }
        return result.toString();
    }

    /**
     * 转换为Double类型
     */
    public static Double toDouble(Object val) {
        if (val == null) {
            return 0D;
        }
        try {
            return Double.valueOf(trim(val.toString()));
        } catch (Exception e) {
            return 0D;
        }
    }

    /**
     * 转换为Float类型
     */
    public static Float toFloat(Object val) {
        return toDouble(val).floatValue();
    }

    /**
     * 转换为Long类型
     */
    public static Long toLong(Object val) {
        return toDouble(val).longValue();
    }

    /**
     * 转换为Integer类型
     */
    public static Integer toInteger(Object val) {
        return toLong(val).intValue();
    }

    /**
     * 获得i18n字符串
     */
	/*public static String getMessage(String code, Object[] args) {
		LocaleResolver localLocaleResolver = (LocaleResolver) SpringContextHolder.getBean(LocaleResolver.class);
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
		Locale localLocale = localLocaleResolver.resolveLocale(request);
		return SpringContextHolder.getApplicationContext().getMessage(code, args, localLocale);
	}*/

    /**
     * 驼峰命名法工具
     *
     * @return toCamelCase(" hello_world ") == "helloWorld"
     * toCapitalizeCamelCase("hello_world") == "HelloWorld"
     * toUnderScoreCase("helloWorld") = "hello_world"
     */
    public static String toCamelCase(String s) {
        if (s == null) {
            return null;
        }

        s = s.toLowerCase();

        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == SEPARATOR) {
                upperCase = true;
            } else if (upperCase) {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    /**
     * 驼峰命名法工具
     *
     * @return toCamelCase(" hello_world ") == "helloWorld"
     * toCapitalizeCamelCase("hello_world") == "HelloWorld"
     * toUnderScoreCase("helloWorld") = "hello_world"
     */
    public static String toCapitalizeCamelCase(String s) {
        if (s == null) {
            return null;
        }
        s = toCamelCase(s);
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    /**
     * 驼峰命名法工具
     *
     * @return toCamelCase(" hello_world ") == "helloWorld"
     * toCapitalizeCamelCase("hello_world") == "HelloWorld"
     * toUnderScoreCase("helloWorld") = "hello_world"
     */
    public static String toUnderScoreCase(String s) {
        if (s == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            boolean nextUpperCase = true;

            if (i < (s.length() - 1)) {
                nextUpperCase = Character.isUpperCase(s.charAt(i + 1));
            }

            if ((i > 0) && Character.isUpperCase(c)) {
                if (!upperCase || !nextUpperCase) {
                    sb.append(SEPARATOR);
                }
                upperCase = true;
            } else {
                upperCase = false;
            }

            sb.append(Character.toLowerCase(c));
        }

        return sb.toString();
    }

    /**
     * 转换为JS获取对象值，生成三目运算返回结果
     *
     * @param objectString 对象串
     *                     例如：row.user.id
     *                     返回：!row?'':!row.user?'':!row.user.id?'':row.user.id
     */
    public static String jsGetVal(String objectString) {
        StringBuilder result = new StringBuilder();
        StringBuilder val = new StringBuilder();
        String[] vals = split(objectString, ".");
        for (int i = 0; i < vals.length; i++) {
            val.append("." + vals[i]);
            result.append("!" + (val.substring(1)) + "?'':");
        }
        result.append(val.substring(1));
        return result.toString();
    }

    /**
     * 将回车换行符转换成html标签
     *
     * @param txt
     * @return
     */
    public static String changeEscapeCharacter2Tag(String txt) {
        if (null != txt) {
            txt = txt.replaceAll("\r\n", "<br>").replaceAll("\r", "<br>").replaceAll("\n", "<br>").replaceAll("\b", "&nbsp;");
        }
        return txt;
    }
	
	/*// 手机号码
	public static boolean checkMobile(String mobileNum) {
		*//*Pattern pattern = Pattern
				.compile("^1(3[0-9]|5[0|1|2|3|5|6|7|8|9]|8[0|2|3|5|6|7|8|9]|4[7])\\d{8}$");*//*

     *//*Pattern pattern = Pattern
				.compile("^0{0,1}(13[0-9]|145|147|15[0-9]|170|17[6-8]|18[0-9])[0-9]{8}$");*//*

		Pattern pattern = Pattern
				.compile("^1\\d{10}$");

		Matcher matcher = pattern.matcher(mobileNum);
		return matcher.matches();
	}*/


    /**
     * 联通
     *
     * @param mobileNum
     * @return
     */
    /*public static boolean checkCUTMobile(String mobileNum) {
        Pattern pattern = compile("^1(3[0-2][0-9]|5[56][0-9]|66[0-9]|709|8[56][0-9]|76[0-9]|45[0-9])\\d{7}$");
        Matcher matcher = pattern.matcher(mobileNum);
        return matcher.matches();
    }*/

    /**
     * 电信
     *
     * @param mobileNum
     * @return
     */
    /*public static boolean checkCTCMobile(String mobileNum) {
        Pattern pattern = compile("^1([35]3[0-9]|8[019][0-9]|7[01375][0-9]|9[0,1,9][0-9])\\d{7}$");
        Matcher matcher = pattern.matcher(mobileNum);
        return matcher.matches();
    }*/

    /**
     * 移动
     *
     * @param mobileNum
     * @return
     */
    /*public static boolean checkCMCMobile(String mobileNum) {
        Pattern pattern = compile("^1(3[4-9][0-9]|5[012789][0-9]|705[0-9]|7[8][0-9]|8[23478][0-9]|4[7][0-9]|9[8][0-9])\\d{7}$");
        Matcher matcher = pattern.matcher(mobileNum);
        return matcher.matches();
    }*/


    /**
     * 判断是否为数字
     */
    public static boolean isNumeric(String str) {
        Matcher isNum = compile("(-|\\+)?[0-9]+(.[0-9]+)?").matcher(str);
        return isNum.matches();
    }

    public static Integer parseInt(Object str) {
        return str == null ? 0 : Integer.valueOf((isNumeric(str.toString())) ? Integer.parseInt(str.toString()) : 0);
    }

    public static Long parseLong(Object str) {
        return str == null ? 0 : Long.valueOf((isNumeric(str.toString())) ? Long.parseLong(str.toString()) : 0);
    }

    /**
     * 将对象转换为 Double 类型
     */
    public static Double parseDouble(Object str) {
        return str == null ? 0 : Double.valueOf((isNumeric(str.toString())) ? Double.parseDouble(str.toString()) : 0);
    }

    /**
     * null 或者 'null' 转换成 ""
     */
    public static final String null2Blank(Object str) {
        return ((null == str || "null".equals(str)) ? "" : str.toString());
    }

    final static DecimalFormat decimalFormat = new DecimalFormat("0");

    /**
     * 数字格式化<br>
     *
     * @param obj
     * @param scale
     * @return
     * @throws Exception
     */
    public static String fmtNumeric(Object obj, int scale) throws Exception {
        String objStr = defaultIfEmpty(null2Blank(obj), "0");
        objStr = ("NaN".equals(objStr) || "Infinity".equals(objStr)) ? "0" : objStr;
        double doubleValue = new BigDecimal(Double.valueOf(objStr).doubleValue()).setScale(scale, BigDecimal.ROUND_HALF_EVEN).doubleValue();

        StringBuffer endWith = new StringBuffer();
        for (int i = 0; i < scale; i++) {
            endWith.append("0");
        }
        String pattern = "0";
        if (endWith.length() > 0) {
            pattern = pattern.concat(".").concat(endWith.toString());
        }
        decimalFormat.applyPattern(pattern);

        return decimalFormat.format(doubleValue);
    }

    public static Byte parseByte(Object str) {
        return str == null ? 0 : Byte.valueOf((isNumeric(str.toString())) ? Byte.parseByte(str.toString()) : 0);
    }

    public static String ClobToString(Clob clob) {
        String reString = "";
        Reader is = null;
        BufferedReader br = null;
        try {
            is = clob.getCharacterStream();// 得到流
            br = new BufferedReader(is);
            String s = br.readLine();
            StringBuffer sb = new StringBuffer();
            while (s != null) {// 执行循环将字符串全部取出付值给 StringBuffer由StringBuffer转成STRING
                sb.append(s);
                s = br.readLine();
            }
            reString = sb.toString();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return reString;

    }

    public static String base64Encode(String source) {

        String str = "";
        try {
            str = new String(Base64.encodeBase64(source.getBytes("UTF-8")), "UTF-8");
        } catch (UnsupportedEncodingException e) {

        }

        return str;
    }

    public static String base64Decode(String source) {

        String str = "";
        try {
            str = new String(Base64.decodeBase64(source.getBytes("UTF-8")), "UTF-8");
        } catch (UnsupportedEncodingException e) {

        }
        return str;
    }

    /**
     * 处理html编码
     */
    public static String unHtmlEncode(String txt) {
        if (null != txt) {
            txt = txt.replace("&amp;", "&").replace("&quot;", "\"").replace("&lt;", "<").replace("&gt;", ">").replace("&nbsp;", " ");
        }
        return txt;
    }


    //判断是否空值
    public static boolean isEmpty(String str) {
        if (null != str && !"".equals(str)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 最大长度字符显示
     */
    public static String leftString(String str, int len, boolean showdot) {
        if (str == null)
            return "";
        if (showdot)
            return StringUtils.cutstring(str, len, "...");
        else
            return StringUtils.cutstring(str, len, "");
    }

    /**
     * 按字节长度截取字符串
     *
     * @param str     将要截取的字符串参数
     * @param toCount 截取的字节长度
     * @param more    字符串末尾补上的字符串
     * @return 返回截取后的字符串
     */
    public static String cutstring(String str, int toCount, String more) {
        int reInt = 0;
        String reStr = "";

        if (str == null)
            return "";
        char[] tempChar = str.toCharArray();
        byte[] CharLen = str.getBytes();
        byte[] moreLen = more.getBytes();

        if (CharLen.length > toCount) {
            toCount = toCount - moreLen.length;
        }

        for (int kk = 0; (kk < tempChar.length && toCount > reInt); kk++) {
            String s1 = String.valueOf(tempChar[kk]);
            byte[] b = s1.getBytes();
            reInt += b.length;
            reStr += tempChar[kk];
        }

        if (CharLen.length > toCount)
            reStr += more;

        return reStr;
    }

    /**
     * 字符串左补位
     *
     * @param str       原字符串
     * @param strLength 长度
     * @return 补位后的字符串
     */
    public static String addZeroForNumLeft(String str, int strLength) {
        return addZeroForNum(str, strLength, 0);
    }

    /**
     * 字符串右补位
     *
     * @param str       原字符串
     * @param strLength 长度
     * @return 补位后的字符串
     */
    public static String addZeroForNumRight(String str, int strLength) {
        return addZeroForNum(str, strLength, 1);
    }

    /**
     * 字符串补位
     *
     * @param str       原字符串
     * @param strLength 长度
     * @param position  位置： 0：左边 1 右边
     * @return 补位后的字符串
     */
    private static String addZeroForNum(String str, int strLength, int position) {
        int strLen = str.length();
        StringBuffer sb = null;
        while (strLen < strLength) {
            sb = new StringBuffer();
            if (position == 1) {
                sb.append(str).append("0");//右补0
            } else if (position == 0) {
                sb.append("0").append(str);// 左补0
            }

            str = sb.toString();
            strLen = str.length();
        }
        return str;
    }

    // 把字符串按照一定字符进行分割
    public static String[] splitString(String szSource, String token) {
        if ((szSource == null) || (token == null))
            return null;
        java.util.StringTokenizer st1 = new java.util.StringTokenizer(szSource,
                token);
        String[] d1 = new String[st1.countTokens()];
        for (int x = 0; x < d1.length; x++)
            if (st1.hasMoreTokens())
                d1[x] = st1.nextToken();
        return d1;
    }

    // 把字符串按照一定字符进行分割 第二种方法
    public static String[] split(String str, String splitsign) {
        int index;
        if (str == null || splitsign == null)
            return null;
        ArrayList<String> al = new ArrayList<String>();
        while ((index = str.indexOf(splitsign)) != -1) {
            al.add(str.substring(0, index));
            str = str.substring(index + splitsign.length());
        }
        al.add(str);
        return (String[]) al.toArray(new String[0]);
    }

    public static byte[] toByte(String szStr) {
        if (szStr == null)
            return null;
        byte[] tmp = szStr.getBytes();
        return tmp;
    }

    public static String toSpace(Object obj) {
        if (obj == null || obj.equals(" "))
            return "&nbsp;";
        return obj.toString();
    }

    public static String toSpace(Date date) {
        if (date == null)
            return "&nbsp;";
        return toString(date);
    }

    public static String toSpace(Date date, String format) {
        if (date == null)
            return "&nbsp;";
        return toString(date, format);
    }

    // 数字数组转换成字符串
    public static String arrayInttoString(int[] ids, String separator) {
        String ret = "";
        for (int i = 0; i < ids.length; i++) {
            if (i < ids.length - 1)
                ret += String.valueOf(ids[i]) + separator;
            else
                ret += String.valueOf(ids[i]);
        }
        return ret;
    }

    // 字符数组转换成字符串
    public static String arrayStrtoString(String[] ids, String separator) {
        String ret = "";
        for (int i = 0; i < ids.length; i++) {
            if (i < ids.length - 1)
                ret += String.valueOf(ids[i]) + separator;
            else
                ret += String.valueOf(ids[i]);
        }
        return ret;
    }

    public static String toString(byte[] byBuf) {
        if (byBuf == null)
            return null;
        return new String(byBuf);
    }

    public static String toString(Object obj) {
        if (obj == null)
            return "";
        return obj.toString().trim();
    }

    public static String toString(String obj) {
        if (obj == null)
            return "";
        return obj;
    }

    public static String toString(Date obj) {
        return toString(obj, "yyyy-MM-dd HH:mm:ss");
    }

    public static String toString(Date obj, String format) {
        if (obj == null)
            return "";
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat(format);
        return df.format(obj);
    }

    /**
     * 最大长度字符显示
     */
    public static String subString(String str, int len) {
        if (str == null)
            return "";
        if (str.length() <= len)
            return str;
        return str.substring(0, len);
    }

    public static String subString(String str, int len, String expandstr) {
        if (str == null)
            return "";
        if (str.length() <= len)
            return str;
        return str.substring(0, len) + expandstr;
    }

    /**
     * 字符串转为list
     *
     * @param str
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<String> str2List(String str) {
        final String URL_SPLIT_PATTERN = "[, ;\r\n|]";// 逗号 空格 分号 换行
        if (str == null) {
            return null;
        }
        String[] arr = str.split(URL_SPLIT_PATTERN);
        List<String> list = new ArrayList<String>();
        for (String a : arr) {
            a = a.trim();
            if (a.length() == 0) {
                continue;
            }
            list.add(a);
        }
        return list;
    }

    /**
     * 字符串转为list
     *
     * @param str
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<Double> str2NumberList(String str) {
        List<String> list = str2List(str);
        List<Double> ret = new ArrayList<Double>();
        for (String s : list) {
            ret.add(Double.valueOf(s));
        }
        return ret;
    }

    //list转字符","
    public static <T> String list2Str(List<T> args, String split) {
        String str = "";
        if (null == split) split = ",";
        if (args != null && args.size() > 0) {
            for (T i : args) {
                str += i + split;
            }
            str = str.substring(0, str.length() - split.length());
            return str;
        } else {
            return "";
        }
    }

    //list转字符","
    public static <T> String array2Str(Integer[] arr, String split) {
        String str = "";
        if (null == split) split = ",";
        if (arr != null && arr.length > 0) {
            for (int i = 0; i < arr.length; i++) {
                str += arr[i] + split;
            }
            str = str.substring(0, str.length() - split.length());
            return str;
        } else {
            return "";
        }
    }

    // 数字显示格式的处理
    public static String toPrice(double d) {
        DecimalFormat df = new DecimalFormat("#,##0.00");
        return df.format(d);
    }

    public static String toPrice(float d) {
        DecimalFormat df = new DecimalFormat("##0.00");
        return df.format(d);
    }

    // 数字显示格式的处理
    public static String toPrice_Fin(double d) {
        DecimalFormat df = new DecimalFormat("###0.00");
        return df.format(d);
    }

    /**
     * <p>方法名: defaultString	</p>
     * <p>描述:	null 或者 "" 返回默认字符串</p>
     * <p>参数:	</p>
     *
     * @return <p>return	String</p>
     */
    public static String defaultString(String originalString, String defaultString) {
        if (null == originalString || "".equals(originalString.trim())) {
            return defaultString;
        }
        return originalString;
    }

    /**
     * 方法名: findDiff <br />
     * 描述:返回oriStr中有的，但str上没有的词 <br />
     * 参数：<br />
     *
     * @param oriStr
     * @param str
     * @return String <br />
     * @throws
     */
    public static String findDiff(String oriStr, String str, String token) {
        if (null == token) token = ",";
        String ret = "";
        if (null == oriStr || 0 == oriStr.length()) {
            return "";
        } else if (null == str || 0 == str.length()) {
            return oriStr;
        } else {
            String[] arr = oriStr.split(token);
            str += token;
            for (String val : arr) {
                if (str.indexOf(val + token) == -1) {
                    ret += val + ",";
                }
            }
            if (ret.length() > 0)
                ret = ret.substring(0, ret.length() - 1);
            return ret;
        }
    }

    // 过滤特殊字符
    public static String StringFilter(String str) throws PatternSyntaxException {
        // 只允许字母和数字
        // String   regEx  =  "[^a-zA-Z0-9]";
        // 清除掉所有特殊字符
        String regEx = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Pattern p = compile(regEx);
        Matcher m = p.matcher(str);
        return m.replaceAll("").trim();
    }

    /**
     * 替换字符串
     *
     * @param text
     * @param repl
     * @param with
     * @return
     */
    public static String replace(String text, String repl, String with) {

        return replace(text, repl, with, -1);
    }

    /**
     * 替换字符串
     *
     * @param text
     * @param repl
     * @param with
     * @param max
     * @return
     */
    public static String replace(String text, String repl, String with, int max) {

        if ((text == null) || (repl == null) || (with == null)
                || (repl.length() == 0) || (max == 0)) {
            return text;
        }

        StringBuffer buf = new StringBuffer(text.length());
        int start = 0;
        int end = 0;
        while ((end = text.indexOf(repl, start)) != -1) {
            buf.append(text.substring(start, end)).append(with);
            start = end + repl.length();

            max--;
            if (max == 0) {
                break;
            }
        }
        buf.append(text.substring(start));
        return buf.toString();
    }

    /**
     * Http replace to Https
     *
     * @param url
     * @return
     */
    public static String replaceHttpToHttps(String url) {
        final String http = "http://";
        final String https = "https://";

        if (StringUtils.isBlank(url) || url.startsWith(https)) {
            return url;
        }

        boolean startsWith = url.indexOf(http) > -1;
        if (startsWith) {
            return url.replaceAll(http, https);
        }

        return url;
    }

    /**
     * 检查是否包含域名
     *
     * @param url
     * @return
     */
    public static boolean checkStartWithHttp(String url) {
        final String http = "http://";
        final String https = "https://";
        if (StringUtils.isBlank(url)) {
            return false;
        }
        return url.startsWith(http) || url.startsWith(https);
    }

    // 过滤怪字符
    public static String filterBadChar(String szText) {
        String tmp = szText;

        String result = "";
        for (int i = 0; i < tmp.length(); i++) {
            char ch = tmp.charAt(i);
            if (ch < ' ' || (ch > '~' && ch < 255))
                continue;
            result += ch;
        }
        return result;
    }

    public static String fixZero(int num) {
        if (num < 10) {
            return "0" + num;
        } else {
            return "" + num;
        }
    }

    /**
     * 获取字符串的长度，如果有中文，则每个中文字符计为2位
     *
     * @param value 指定的字符串
     * @return 字符串的长度
     */
    public static int getLength(String value) {
        int valueLength = 0;
        String chinese = "[\u0391-\uFFE5]";
        /* 获取字段值的长度，如果含中文字符，则每个中文字符长度为2，否则为1 */
        for (int i = 0; i < value.length(); i++) {
            /* 获取一个字符 */
            String temp = value.substring(i, i + 1);
            /* 判断是否为中文字符 */
            if (temp.matches(chinese)) {
                /* 中文字符长度为2 */
                valueLength += 2;
            } else {
                /* 其他字符长度为1 */
                valueLength += 1;
            }
        }
        return valueLength;
    }

    // 过滤特殊字符
    public static String filterEmoji(String str) throws PatternSyntaxException {
        if (str.trim().isEmpty()) {
            return str;
        }
		/*String pattern="[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]";
		String reStr="";
		Pattern emoji=Pattern.compile(pattern);
		Matcher  emojiMatcher=emoji.matcher(str);
		str=emojiMatcher.replaceAll(reStr);*/
        return str.replaceAll("[^\\u0000-\\uFFFF]", "");
    }

    /**
     * 合拼字符串
     *
     * @param split
     * @param args
     * @return
     */
    public static String concat(String split, Object... args) {
        StringBuilder sb = new StringBuilder();
        for (Object arg : args) {
            sb.append("" + arg).append(split);
        }
        return sb.toString().substring(0, sb.toString().length() - split.length());
    }

    /**
     * 根据Unicode编码判断中文汉字和符号
     *
     * @param c
     * @return
     */
    private static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION) {

            return true;
        }
        return false;
    }

    /**
     * 完整的判断中文汉字和符号
     *
     * @param strName
     * @return
     */
    public static boolean isChinese(String strName) {
        char[] ch = strName.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            char c = ch[i];
            if (isChinese(c)) {
                return true;
            }
        }
        return false;

    }

    public static List<String> getImageSrc(String htmlStr) {
        List<String> pics = new ArrayList<String>();
        String img = "";
        Pattern p_image;
        Matcher m_image;
        //     String regEx_img = "<img.*src=(.*?)[^>]*?>"; //图片链接地址
        String regEx_img = "<img.*src\\s*=\\s*(.*?)[^>]*?>";
        p_image = compile
                (regEx_img, CASE_INSENSITIVE);
        m_image = p_image.matcher(htmlStr);
        while (m_image.find()) {
            // 得到<img />数据
            img = m_image.group();
            // 匹配<img>中的src数据
            Matcher m = compile("src=[\\'\\\"](.*?)[\\'\\\"]").matcher(img);
            while (m.find()) {
                pics.add(m.group(1));
            }
        }
        return pics;
    }

    /**
     * 判断是否含有特殊字符
     *
     * @param str
     * @return true为包含，false为不包含
     */
    public static boolean isSpecialChar(String str) {
        String regEx = "[ _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t";
        Pattern p = compile(regEx);
        Matcher m = p.matcher(str);
        return m.find();
    }

    /**
     * 用getBytes(encoding)：返回字符串的一个byte数组 当b[0]为 63时，应该是转码错误 A、不乱码的汉字字符串： 1、encoding用UTF-8时，每byte是负数； 2、encoding用ISO8859_1时，b[i]全是63。 B、乱码的汉字字符串： 1、encoding用ISO8859_1时，每byte也是负数； 2、encoding用UTF-8时，b[i]大部分是63。 C、英文字符串 1、encoding用ISO8859_1和UTF-8时，每byte都大于0；
     * <p/>
     * 总结：给定一个字符串，用getBytes("iso8859_1") 1、如果b[i]有63，不用转码； A-2 2、如果b[i]全大于0，那么为英文字符串，不用转码； B-1 3、如果b[i]有小于0的，那么已经乱码，要转码。 C-1
     */
    public static String toUTF8(String str) {
        if (str == null)
            return null;
        String retStr = str;
        byte b[];
        try {
            b = str.getBytes("ISO8859_1");

            for (int i = 0; i < b.length; i++) {
                byte b1 = b[i];
                if (b1 == 63)
                    break; // 1
                else if (b1 > 0)
                    continue;// 2
                else if (b1 < 0) { // 不可能为0，0为字符串结束符
                    retStr = new String(b, "UTF-8");
                    break;
                }
            }
        } catch (UnsupportedEncodingException e) {
            // e.printStackTrace(); //To change body of catch statement use File
            // | Settings | File Templates.
        }
        return retStr;
    }

    /**
     * 把字符串split分割后转化为map
     *
     * @param split 字符串
     * @param regex 分隔符
     * @return
     */
    public static Map<String, String> getMapSplit(String split, String regex) {
        Map<String, String> map = new HashMap();
        if (StringUtils.isEmpty(split)) {
            return null;
        }
        if (StringUtils.isEmpty(regex)) {
            return null;
        }
        String[] data = split.split(regex);

        for (int i = 0; i < data.length; i++) {
            int index = data[i].indexOf('=');
            map.put(data[i].substring(0, index), data[i].substring((index + 1)));
        }

        return map;

    }

    /**
     * 判断字符串是否纯英文
     *
     * @param str
     * @return
     */
    public static boolean isEnglish(String str) {
        return str.matches("^[a-zA-Z]*");
    }

    /**
     * 手机号格式验证
     *
     * @param phoneNumber 手机号
     * @return
     */
    /*public static boolean isPhoneNumber(String phoneNumber) {
        if (checkCMCMobile(phoneNumber) || checkCTCMobile(phoneNumber) || checkCUTMobile(phoneNumber)) {
            return true;
        }
    }*/

    /**
     * 根据路径获取query参数
     *
     * @param url 当前的路径
     * @param key query对应key
     * @return query key 对应的 value
     */
    public static String getParam(String url, String key) {
        if (StringUtils.isBlank(url)) {
            return "";
        }
        url = url.substring(url.indexOf("?") + 1).replace("?", "&");
        String[] params = url.split("&");
        for (String param : params) {
            if (param.startsWith(key + "=")) {
                return param.replaceFirst(key + "=", "").trim();
            }

        }
        return "";
    }

    /**
     * 替换url 对应的Query值
     *
     * @param url   当前的路径
     * @param key   query对应key
     * @param value query key 对应的 value
     * @return
     */
    public static String replaceQueryParams(String url, String key, String value) {
        if (StringUtils.isNotBlank(url) && StringUtils.isNotBlank(value)) {
            int index = url.indexOf(key + "=");
            if (index != -1) {
                String[] params = url.split(key + "=");
                if (params.length > 1) {
                    String oldValue = params[1].split("&")[0];
                    params[1] = params[1].replaceFirst(oldValue, key + "=" + value);
                    return join(params, "");
                } else {
                    return url + value;
                }

            }

        }
        return url;
    }

    /**
     * 判断是否微信客户端
     *
     * @param userAgent
     * @return
     */
    public static boolean isWeChat(String userAgent) {
        if (StringUtils.isBlank(userAgent)) {
            return false;
        }

        return userAgent.toLowerCase().indexOf("micromessenger") > -1;
    }

    /***
     * 下划线命名转为驼峰命名
     *
     * @param para
     *        下划线命名的字符串
     */
    public static String underlineToHump(String para) {
        StringBuilder result = new StringBuilder();
        String a[] = para.split(UNDERLINE);
        for (String s : a) {
            if (!para.contains(UNDERLINE)) {
                result.append(s);
                continue;
            }
            if (result.length() == 0) {
                result.append(s.toLowerCase());
            } else {
                result.append(s.substring(0, 1).toUpperCase());
                result.append(s.substring(1).toLowerCase());
            }
        }
        return result.toString();
    }

    /***
     * 驼峰命名转为下划线命名
     *
     * @param para
     *        驼峰命名的字符串
     */

    public static String humpToUnderline(String para) {
        StringBuilder sb = new StringBuilder(para);
        int temp = 0;//定位
        if (!para.contains(UNDERLINE)) {
            for (int i = 0; i < para.length(); i++) {
                if (Character.isUpperCase(para.charAt(i))) {
                    sb.insert(i + temp, UNDERLINE);
                    temp += 1;
                }
            }
        }
        return sb.toString().toLowerCase();
    }


    // 判断一个字符串是否含有数字
    public static boolean HasDigit(String content) {
        boolean flag = false;
        Pattern p = compile(".*\\d+.*");
        Matcher m = p.matcher(content);
        if (m.matches()) {
            flag = true;
        }
        return flag;
    }

    /**
     * 校验ip格式
     *
     * @param ip 地址
     * @return
     */
    public static boolean isCorrectIP(String ip) {
        if (ip.length() < 7 || ip.length() > 15 || "".equals(ip)) {
            return false;
        }
        /**
         * 判断IP格式和范围
         */
        String rexp = "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$";
        Pattern pat = Pattern.compile(rexp);

        Matcher mat = pat.matcher(ip);

        boolean ipAddress = mat.find();

        return ipAddress;
    }

    /**
     * 判断对象是否为空
     *
     * @param obj
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static boolean isNull(Object obj) {
        boolean flag = false;
        if (obj == null) {/* 一般对象 */
            flag = true;
        } else if (obj instanceof Collection && ((Collection) obj).isEmpty()) {/* 没装数据的集合List */
            flag = true;
        } else if (obj instanceof Map && ((Map) obj).isEmpty()) {/* 没装数据的Map */
            flag = true;
        } else if ("".equals((obj + "").trim())
                || "null".equals((obj + "").trim())) {/* 字符串判断 */
            flag = true;
        } else if (obj.getClass().isArray() && Array.getLength(obj) <= 0) {/* 数组判断 */
            flag = true;
        }
        return flag;
    }


    /**
     * 截取域名部分
     *
     * @param url
     * @return
     */
    public static String getDomainForUrl(String url) {
        if (!checkStartWithHttp(url)) {
            return "";
        }
        String re = "((http|ftp|https)://)(([a-zA-Z0-9._-]+)|([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}))(([a-zA-Z]{2,6})|(:[0-9]{1,4})?)";
        String str = "";
        Pattern pattern = Pattern.compile(re);
        // 忽略大小写的写法
        // Pattern pat = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(url);
        if (!matcher.matches()) {
            String[] split2 = url.split(re);
            if (split2.length > 1) {
                String substring = url.substring(0, url.length() - split2[1].length());
                str = substring;
            } else {
                str = split2[0];
            }
        }
        return str;
    }

    /**
     * 截取除了域名部分
     *
     * @param picUrl
     * @return
     */
    public static String getDomainAfterLastPicUrl(String picUrl) {
        if (StringUtils.isBlank(picUrl)) {
            return picUrl;
        }
        if (!StringUtils.checkStartWithHttp(picUrl)) {
            return StringUtils.replace(picUrl, "//", "/");
        }
        String domainForUrl = StringUtils.getDomainForUrl(picUrl);
        String result = StringUtils.substringAfterLast(picUrl, domainForUrl);
        return StringUtils.replace(result, "//", "/");
    }


    /**
     * 判断字符串是否含有Emoji表情
     **/
    public static boolean containsEmoji(String str) {
        if (isEmpty(str)) {
            return false;
        }
        String regEx = "[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]";
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(str);
        return matcher.find();
    }

    /**
     * 截取字符串并在末尾追加字符串
     * @param str 原字符串
     * @param maxLength 截取长度
     * @param appendStr 追加字符串
     */
    public static String cutAndAppendStr(String str, int maxLength, String appendStr) {
        if (StringUtils.isEmpty(str)) {
            return StringUtils.EMPTY;
        }
        if (str.length() > maxLength) {
            return str.substring(0, maxLength) + StringUtils.null2Blank(appendStr);
        }
        return str;
    }

    public static void main(String[] args) {

//        System.out.println(JsonMapper.toJsonString(StringUtils.getImageSrc("<img src=\"/mall/upfile/mypro/585a2f905bc30.png\"/><img src=\"/mall/upfile/mypro/585a2fc5d5d0e.jpg\"/><img src=\"/mall/upfile/mypro/585a2fb6a6b38.gif\"/><img src=\"/mall/upfile/mypro/585a2fe50743e.gif\"/><img src=\"/mall/upfile/mypro/585a2fecc474a.jpg\"/><img src=\"/mall/upfile/mypro/585a2feccc863.gif\"/><img src=\"/mall/upfile/mypro/585a2fecd7a12.gif\"/><img src=\"/mall/upfile/mypro/585a2fecef851.gif\"/><img src=\"/mall/upfile/mypro/585a2fece6d33.gif\"/><img src=\"/mall/upfile/mypro/585a2ff64cc94.gif\"/><img src=\"/mall/upfile/mypro/585a2ff66e4ee.gif\"/><img src=\"/mall/upfile/mypro/585a2ff655723.jpg\"/><img src=\"/mall/upfile/mypro/585a2ff6754eb.gif\"/><img src=\"/mall/upfile/mypro/585a2ff661af5.jpg\"/><img src=\"/mall/upfile/mypro/585a2ff67b0bc.gif\"/><img src=\"/mall/upfile/mypro/585a2ff66844d.gif\"/><img src=\"/mall/upfile/mypro/585a2ffd00bbc.jpg\"/><img src=\"/mall/upfile/mypro/585a2ff68022a.gif\"/><img src=\"/mall/upfile/mypro/585a2ff6cd5fd.gif\"/><img src=\"/mall/upfile/mypro/585a2ff687161.jpg\"/>")));

        String str1 = "12345";
        System.out.println(StringUtils.cutstring(str1, 4, "0"));

        System.out.println(humpToUnderline("humpToUnderline"));
    }


}
