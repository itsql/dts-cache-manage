package com.dg.mall.cache.config;

import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description: redis配置类
 * @Created: with IntelliJ IDEA.
 * @author: wuwen
 * @createTime: 2021-06-23 20:24
 **/
@Slf4j
@Configuration
@AutoConfigureOrder(value = 99)
public class MallCacheRedisConfig {
	
	@Bean
	public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
	    RedisTemplate<String, Object> redisTemplate = new RedisTemplate<String, Object>();
	    
	    redisTemplate.setConnectionFactory(connectionFactory);
	    // 字符串key序列化方式
	    redisTemplate.setKeySerializer(RedisSerializer.string());
	    // Hash key序列化方式
	    redisTemplate.setHashKeySerializer(RedisSerializer.string());
	    // 设置序列化方式, 前面只是声明, 该方法才实际注入序列化方式
	    redisTemplate.afterPropertiesSet();
	    log.info("初始化redisTemplate成功。");
	    return redisTemplate;
	}
	
	
	@Bean
	@ConditionalOnMissingBean
	public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory connectionFactory) {
		 StringRedisTemplate redisTemplate = new StringRedisTemplate();
	     redisTemplate.setConnectionFactory(connectionFactory);
	     // 字符串key序列化方式
	     redisTemplate.setKeySerializer(RedisSerializer.string());
	     // 字符串value序列化方式
	     redisTemplate.setValueSerializer(RedisSerializer.string());
	     // Hash key序列化方式
	     redisTemplate.setHashKeySerializer(RedisSerializer.string());
	     // Hash value序列化方式
	     redisTemplate.setHashValueSerializer(RedisSerializer.string());
	     // 设置序列化方式, 前面只是声明, 该方法才实际注入序列化方式
	     redisTemplate.afterPropertiesSet();
	     log.info("初始化stringRedisTemplate成功。");
		 return redisTemplate;
	 }
	
	
	

	
}


