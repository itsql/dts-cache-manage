package com.dg.mall.cache.config;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import com.aliyun.dts.subscribe.clients.ConsumerContext;
import com.aliyun.dts.subscribe.clients.DTSConsumer;
import com.aliyun.dts.subscribe.clients.DefaultDTSConsumer;
import com.aliyun.dts.subscribe.clients.common.RecordListener;
import com.dg.mall.cache.dts.listener.ClusterDTSListener;
import com.dg.mall.cache.dts.service.ClusterDTSService;
import com.dg.mall.cache.dts.store.ClusterDTSMetaStore;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: wuwen
 * @createTime: 2021-05-28 18:53
 **/
@Data
@Slf4j
@Configuration
@ConfigurationProperties(prefix = "dts")
public class MallCacheDTSConfig {
	
    //地域
    private String brokerUrl;
    
    //数据订阅通道的订阅Topic。
    private String topic;
    
    //数据订阅实例ID
    private String sid;
    
    //消费组的账号。
    private String userName;
    
    //该账号的密码。
    private String password;
    
    //消费位点
    private String initCheckpoint;
    
    @Autowired
    private ClusterDTSListener clusterDTSListener;
    
    @Autowired
    private ClusterDTSMetaStore clusterDTSMetaStore;
    
    @Autowired
	private ApplicationContext  applicationContext;
    
    //需要订阅的table
	public static ConcurrentHashMap<String, Class<?>> subscribeMap=new ConcurrentHashMap<String, Class<?>>();
    
	@PostConstruct
    public void consumerContext() {
		
		ConsumerContext consumerContext = new ConsumerContext(brokerUrl, topic, sid, userName, password, "1626854374614", ConsumerContext.ConsumerSubscribeMode.SUBSCRIBE);
		 
		//SUBSCRIBE模式 setForceUseCheckpoint配置不生效
		//consumerContext.setForceUseCheckpoint(true);
		
		consumerContext.setUserRegisteredStore(clusterDTSMetaStore);

		DTSConsumer dtsConsumer = new DefaultDTSConsumer(consumerContext);

		dtsConsumer.addRecordListeners(buildRecordListener());
		
		//注册自定义订阅消费服务
		registerDTSService();
		
		//此处用异步启动，DTS的官网start会阻塞主线程。
		CompletableFuture.runAsync(()-> {
			try {
				dtsConsumer.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		});
    }
	
	/**
	 * 此处保留Map，后期可以同时添加订阅通道
	 * @return
	 */
	public Map<String, RecordListener> buildRecordListener() {
	    return Collections.singletonMap("mysqlRecordPrinter", clusterDTSListener);
	}


	/**
	 * 自定义注册DTS订阅
	 * 本项目中只在继承了DTSService类就表示需要订阅此表的DDL数据
	 * 2021年7月12日10:34:18
	 */
	@SuppressWarnings("rawtypes")
	private void registerDTSService() {
		Map<String, ClusterDTSService> mapDTSService=applicationContext.getBeansOfType(ClusterDTSService.class);
		mapDTSService.keySet().parallelStream().forEach(key->{
			 String tableName=mapDTSService.get(key).getTableName();
    		 Class<?> clazz=mapDTSService.get(key).getClass();
    		 subscribeMap.put(tableName, clazz);
    		 log.info("{}：表注册DTS订阅成功，消费服务：{}",tableName,clazz);
		});
	} 
	
}



	
	

