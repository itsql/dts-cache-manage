package com.dg.mall.cache.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description: redission配置类
 * @Created: with IntelliJ IDEA.
 * @author: wuwen
 * @createTime: 2021-06-23 20:24
 **/

@Slf4j
@Configuration
public class MallCacheRedissionConfig {
	
	 	@Autowired
	 	private RedisProperties redisProperties;
	 	
	    @Bean
	    public RedissonClient redissonClient() {
	    	log.error(JSONObject.toJSONString(redisProperties));
	    	
	        Config config = new Config();
	        String url = "redis://" + redisProperties.getHost() + ":" + redisProperties.getPort();
	        // 这里以单台redis服务器为例
	        config.useSingleServer().setAddress(url).setDatabase(redisProperties.getDatabase());
	        
	        // 实际开发过程中应该为cluster或者哨兵模式，这里以cluster为例
	        //String[] urls = {"127.0.0.1:6379", "127.0.0.2:6379"};
	        //config.useClusterServers().addNodeAddress(urls).setPassword(redisProperties.getPassword()).setDatabase(redisProperties.getDatabase());
	        
	        RedissonClient redissonClient=Redisson.create(config);
	    	log.error("初始化redissonClient成功。");
	    	
	        return redissonClient;
	    }
}


