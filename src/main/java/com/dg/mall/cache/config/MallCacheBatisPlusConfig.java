package com.dg.mall.cache.config;

import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: wuwen
 * @createTime: 2021-05-28 18:53
 **/

@Configuration
@AutoConfigureOrder(value = 1)
public class MallCacheBatisPlusConfig {

	 /**
	  *  新的分页插件,一缓和二缓遵循 mybatis 的规则
     */
	@Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
		 MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
	     interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
	     return interceptor;
    }

}
