package com.dg.mall.cache.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.dg.mall.cache.interceptor.LogInterceptor;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: wuwen
 * @createTime: 2021-06-23 20:28
 **/
@Configuration
public class MallCacheInterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getLogInterceptor());
    }
    
    public HandlerInterceptor getLogInterceptor() {
        return new LogInterceptor();
    }



}
