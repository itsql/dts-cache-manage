package com.dg.mall.cache.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dg.mall.cache.filter.ApiAuthenticationFilter;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: wuwen
 * @createTime: 2021-06-23 20:28
 **/
@Configuration
public class MallCacheFilterConfig {

	
	@Bean
    public FilterRegistrationBean<ApiAuthenticationFilter> registApiAuthenticationFilter() {
        FilterRegistrationBean<ApiAuthenticationFilter> registration = new FilterRegistrationBean<ApiAuthenticationFilter>();
        registration.setFilter(new ApiAuthenticationFilter());
        registration.addUrlPatterns("/*");
        registration.setName("ApiAuthenticationFilter");
        registration.setOrder(1);
        return registration;
    }



}
