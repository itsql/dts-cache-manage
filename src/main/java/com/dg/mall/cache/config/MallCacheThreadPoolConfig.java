package com.dg.mall.cache.config;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * @Description: 线程池配置类
 * @Created: with IntelliJ IDEA.
 * @author: wuwen
 * @createTime: 2021-06-23 20:24
 **/

@Data
@Configuration
@ConfigurationProperties(prefix = "thread.pool")
public class MallCacheThreadPoolConfig {
	
	private Integer coreSize;

	private Integer maxSize;

	private Integer keepAliveTime;
	
    @Bean
    public ThreadPoolExecutor threadPoolExecutor() {
        return new ThreadPoolExecutor(
        		coreSize,
        		maxSize,
        		keepAliveTime,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(100000),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()
        );
    }

}


