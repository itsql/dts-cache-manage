package com.dg.mall.cache.config;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.xpack.client.PreBuiltXPackTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.util.CollectionUtils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description: ElasticSearch配置类
 * @Created: with IntelliJ IDEA.
 * @author: wuwen
 * @createTime: 2021-06-23 20:24
 **/

@Slf4j
@Data
@Configuration
@ConfigurationProperties(prefix = "spring.data.elasticsearch")
@EnableElasticsearchRepositories("com.dg.mall.cache.elasticsearch")
public class MallCacheElasticSearchConfig {
	
	//端口号
    private int port;

    //集群名称
    private String clusterName;
    
    //用户信息
    private String xpackUser;
    
    //连接池
    private String poolSize;
    
    //集群的地址
    @Value("#{'${hosts:localhost}'.split(',')}")
    private List<String> hosts = new ArrayList<>();
    
    //初始化配置
    private Settings settings() {
        Settings settings = Settings.builder()
                .put("cluster.name", clusterName)
                 //.put("xpack.security.user", xpackUser )
                .put("thread_pool.search.size", Integer.parseInt(poolSize))
                .put("client.transport.sniff", false).build();
        return settings;
    }
 
    @Bean
    protected TransportClient transportClient() {
		TransportClient transportClient = new PreBuiltXPackTransportClient(settings());
        if (!CollectionUtils.isEmpty(hosts)) {
            hosts.stream().forEach(h -> {
                try {
                    transportClient.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(h), port));
                } catch (UnknownHostException e) {
                	log.error(e.getMessage());
                }
            });
        }
        log.info("初始化transportClient成功。");
        return transportClient;  
    }
    
    
    @Bean
    public ElasticsearchOperations elasticsearchOperations() {
        return new ElasticsearchTemplate(transportClient());
    }

    @Bean
    public ElasticsearchTemplate elasticsearchTemplate(TransportClient transportClient) {
        return new ElasticsearchTemplate(transportClient);
    }
 
}


