package com.dg.mall.cache.modules.elasticsearch.base;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;

import org.elasticsearch.client.transport.TransportClient;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.AliasQuery;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.dg.mall.cache.common.utils.ArrayUtils;

import cn.hutool.core.collection.CollectionUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Transactional(readOnly = false)
public class BaseElasticSearchService<D extends ElasticsearchRepository<T,ID>, T extends BaseElasticSearchModel, ID extends Serializable> {
	
	@Autowired
	protected D repository;
	
	@Autowired
	protected TransportClient transportClient;
	
	@Autowired
	protected ElasticsearchTemplate elasticsearchTemplate;
	
	@Autowired	
	protected ThreadPoolExecutor threadPoolExecutor;
	
	@Autowired
	protected RedisTemplate<String, Object> redisTemplate;
	
	@Autowired
	protected RedissonClient redissonClient;
	
	
	public Class<T> getEntityClass(){
		return repository.getEntityClass();
	}
	
	public void save(T t) {
		repository.save(t);
	}

	public void saveAll(List<T> list) {
		repository.saveAll(list);
	}
	
	
	/**
	 * 开启多线程异步保存数据ES
	 */
	public void saveAsync(T data) {
		//开启多线程异步保存数据ES
		CompletableFuture.runAsync(()-> {
            	if(data!=null) {
            		try {
            			repository.save(data);
					} catch (Exception e) {
						log.error("保存ES失败：{}",e.getMessage());
					}
            	}
	     },threadPoolExecutor);
	}
	
	
	/**
	 * 开启多线程异步保存数据ES
	 */
	public void saveAllAsync(List<T> list) {
		if(CollectionUtil.isNotEmpty(list)) {
			//开启多线程异步保存数据ES
			CompletableFuture.completedFuture(list)
		            .thenAcceptAsync(data -> {
		            	if(CollectionUtil.isNotEmpty(data)) {
		            		repository.saveAll(data);
		            	}
		            },threadPoolExecutor);
			
		}
	}
	
	
	/**
	 * 当一次性保存的数据量比较大时，对大数组进行拆分多小数组，同时多开线程保存ES
	 * batchSize: 拆分的子数组大小数量
	 */
	public void saveBatchAsync(List<T> list,Integer batchSize) {
		if(CollectionUtil.isNotEmpty(list)) {
			if(list.size()>batchSize) {
				List<List<T>> spliceList=ArrayUtils.spliceArrays(list, batchSize);
					//并行流处理
					spliceList.parallelStream().forEach(volist->{
						//开启多线程异步保存数据ES
						CompletableFuture.completedFuture(volist)
					            .thenAcceptAsync(data -> {
					            	if(CollectionUtil.isNotEmpty(data)) {
					            		this.saveAll(data);
					            	}
					            },threadPoolExecutor);
						
					});
			}else {
	        	this.saveAll(list);
			}
		}
	}
	
	public void delete(T t) {
		repository.delete(t);
	}

	public void deleteById(ID id) {
		repository.deleteById(id);
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
	
	public long count() {
		return repository.count();
	}
	
	public T getById(ID id) {
		return repository.findById(id).get();
	}
	
	public boolean  existsById(ID id) {
		return repository.existsById(id);
	}
	
	public Iterable<T> findAll() {
		return repository.findAll();
	}
	
	public Page<T> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}
	
	public Iterable<T> findAll(Sort sort) {
		return repository.findAll(sort);
	}
	
	public boolean indexExists() {
		return elasticsearchTemplate.indexExists(repository.getEntityClass());
	}
	
	public boolean deleteIndex() {
		return elasticsearchTemplate.deleteIndex(repository.getEntityClass());
	}
	
	public boolean createIndex() {
		return elasticsearchTemplate.createIndex(repository.getEntityClass());
	}
	
	public boolean addAlias(String indexName,String... aliases) {
		for (String aliase : aliases) {
			AliasQuery query=new AliasQuery();
			query.setIndexName(indexName);
			query.setAliasName(aliase);
			query.setSearchRouting(aliase);
			elasticsearchTemplate.addAlias(query);
		}
		return true;
	}
	
	public boolean removeAlias(String indexName,String... aliases) {
		for (String aliase : aliases) {
			AliasQuery query=new AliasQuery();
			query.setIndexName(indexName);
			query.setAliasName(aliase);
			elasticsearchTemplate.removeAlias(query);
		}	
		return true;
	}
	
	
	
	
	
	
	
	
	
	
}
