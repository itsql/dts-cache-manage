package com.dg.mall.cache.modules.elasticsearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.dg.mall.cache.modules.elasticsearch.model.MallProdureVo;

@Repository
public interface MallProdureESRepository extends ElasticsearchRepository<MallProdureVo, Integer> {
	
	
	
	
}
