package com.dg.mall.cache.modules.elasticsearch.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.dg.mall.cache.common.constant.ElasticSearchConstant;
import com.dg.mall.cache.modules.elasticsearch.base.BaseElasticSearchModel;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@Document(indexName = ElasticSearchConstant.DTS_MESSAGE_LOG_INDEXNAME, type = ElasticSearchConstant.DTS_MESSAGE_LOG__INDEXTYPE, createIndex = true )
public class MessageDTSLogVo extends BaseElasticSearchModel{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Field(type = FieldType.Long,store = true)
	private Long id;			//UUID
	
	@Field(type = FieldType.Long,store = true)
    private Long timestamp;   		// 消费时间点
	
	@Field(type = FieldType.Integer,store = true)
    private Integer partition;   		// 消费位点
	
	@Field(type = FieldType.Keyword,store = true)
    private String topic;   		// 队列名称
	
	@Field(type = FieldType.Keyword,store = true)
    private String databaseName;          // 数据库名称
	
	@Field(type = FieldType.Keyword,store = true)
    private String tableName;          // 数据库表名称
    
    @Field(type = FieldType.Keyword,store = true)
    private String primaryKey;        // 主键列名称
    
    @Field(type = FieldType.Keyword,store = true)
    private String messageType;        // 消息的类型
    
    @Field(type = FieldType.Text,store = true)
    private String messageOld;        // 消息的主体,当类型为update时，此字段有值

    @Field(type = FieldType.Text,store = true)
    private String message;        // 消息的主体
    
    @Field(type = FieldType.Date, pattern = "yyyy-MM-dd HH:mm:ss", format = DateFormat.custom)
    @JsonFormat(shape =JsonFormat.Shape.STRING,pattern ="yyyy-MM-dd HH:mm:ss",timezone ="GMT+8")
    private Date createTime;	//创建时间
    
    @Field(type = FieldType.Date, pattern = "yyyy-MM-dd HH:mm:ss", format = DateFormat.custom)
    @JsonFormat(shape =JsonFormat.Shape.STRING,pattern ="yyyy-MM-dd HH:mm:ss",timezone ="GMT+8")
    private Date successTime;	//消费成功
    
    @Field(type = FieldType.Text,store = true)
    private String exception;	//发生的异常消息
    
  
}
