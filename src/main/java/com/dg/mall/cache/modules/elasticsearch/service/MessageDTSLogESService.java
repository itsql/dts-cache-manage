package com.dg.mall.cache.modules.elasticsearch.service;

import org.redisson.api.RLock;
import org.springframework.stereotype.Service;

import com.dg.mall.cache.common.constant.ElasticSearchConstant;
import com.dg.mall.cache.common.utils.StringUtils;
import com.dg.mall.cache.modules.elasticsearch.base.BaseElasticSearchService;
import com.dg.mall.cache.modules.elasticsearch.model.MessageDTSLogVo;
import com.dg.mall.cache.modules.elasticsearch.repository.MessageDTSLogESRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MessageDTSLogESService extends BaseElasticSearchService<MessageDTSLogESRepository, MessageDTSLogVo, Long> {
	
	/**
	 * 重建index,type
	 */
	public void rebuildIndex() {
		//加锁,防止同时重建
		RLock rlock = redissonClient.getLock(this.getEntityClass().getName());
		try {
	        if (rlock.tryLock()) {
				if(this.indexExists()) {
					this.deleteIndex();
					log.info("重建DTSMessageLog索引：删除旧索引");
				}
				
				this.createIndex();
				//设置别名
				if(StringUtils.isNotEmpty(ElasticSearchConstant.PRODUCT_INDEXNAME)) {
					this.addAlias(ElasticSearchConstant.PRODUCT_INDEXNAME, ElasticSearchConstant.PRODUCT_ALIASES);
				}
				log.info("重建DTSMessageLog索引：成功");
	        }
		} finally {
			 if(rlock.isHeldByCurrentThread()){
				 rlock.unlock();
			 }
		}
	}
	
}
