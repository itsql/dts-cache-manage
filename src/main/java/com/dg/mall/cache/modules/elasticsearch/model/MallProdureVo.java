package com.dg.mall.cache.modules.elasticsearch.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.dg.mall.cache.common.constant.ElasticSearchConstant;
import com.dg.mall.cache.modules.elasticsearch.base.BaseElasticSearchModel;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@Document(indexName = ElasticSearchConstant.PRODUCT_INDEXNAME, type = ElasticSearchConstant.PRODUCT_INDEXTYPE, createIndex = true )
public class MallProdureVo extends BaseElasticSearchModel{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Field(type = FieldType.Integer,store = true)
	private Integer fdMproId;			//商品ID
	
	@Field(type = FieldType.Keyword)
    private String fdMproName;          // 商品名称
    
	@Field(type = FieldType.Integer,store = true)
    private Integer fdMproSalenum;        // 销售数量
    
	@Field(type = FieldType.Double,store = true)
    private Double fdMproPrice;        // 商品商城价
    
	@Field(type = FieldType.Keyword,store = true)
    private String fdMproPic;        // 商品主图
    
	@Field(type = FieldType.Text,store = true)
    private String fdMskuTitle;        // 标题
    
	@Field(type = FieldType.Integer,store = true)
    private Integer fdMproExpresstempletId; // 运费模板

    @Field(type = FieldType.Keyword)
    private String categoryName;//三级类目名称
    
    @Field(type = FieldType.Integer,store = true)
    private String categoryid;//三级级类目id

    @Field(type = FieldType.Integer,store = true)
    private String scategoryid;//二级级类目id
    
    @Field(type = FieldType.Keyword)
    private String scategoryName;//二级类目名称

    @Field(type = FieldType.Integer,store = true)
    private String fcategoryid;//一级级类目id
    
    @Field(type = FieldType.Keyword)
    private String fcategoryName;//一级类目名称
    


}
