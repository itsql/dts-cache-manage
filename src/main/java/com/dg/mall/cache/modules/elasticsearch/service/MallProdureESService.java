package com.dg.mall.cache.modules.elasticsearch.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dg.mall.cache.common.constant.ElasticSearchConstant;
import com.dg.mall.cache.common.utils.StringUtils;
import com.dg.mall.cache.converter.MallProdureConverter;
import com.dg.mall.cache.entity.MallProdure;
import com.dg.mall.cache.modules.elasticsearch.base.BaseElasticSearchService;
import com.dg.mall.cache.modules.elasticsearch.model.MallProdureVo;
import com.dg.mall.cache.modules.elasticsearch.repository.MallProdureESRepository;
import com.dg.mall.cache.service.MallProdureService;

import cn.hutool.core.collection.CollectionUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MallProdureESService extends BaseElasticSearchService<MallProdureESRepository, MallProdureVo, Integer> {
	
	@Autowired	
	private MallProdureService mallProdureService;
	
	//加载一页的数据
	private final static int pageSize=1000;
	
	//并发处理数据的线程数量
	private final static int threadSize=20;
	
	//页码缓存KEY
	private final static String loadPageKey="MPPAGE_KEY";
	
	//是否重新全量加载
	private final static boolean isInitLoad=true;
	
	/**
	 * 重建index,type
	 */
	public void rebuildIndex() {
		//加锁,防止同时重建
		RLock rlock = redissonClient.getLock(this.getEntityClass().getName());
		try {
	        if (rlock.tryLock()) {
				if(this.indexExists()) {
					this.deleteIndex();
					log.info("重建MallProdure索引：删除旧索引");
				}
				this.createIndex();
				//设置别名
				if(StringUtils.isNotEmpty(ElasticSearchConstant.PRODUCT_ALIASES)) {
					this.addAlias(ElasticSearchConstant.PRODUCT_INDEXNAME, ElasticSearchConstant.PRODUCT_ALIASES);
				}
				log.info("重建MallProdure索引：成功");
	        }
		} finally {
			 if(rlock.isHeldByCurrentThread()){
				 rlock.unlock();
			 }
		}
	}
	
	
	/**
	 * 全量更新同步数据库数据
	 */
	@SuppressWarnings("unused")
	public void loadAllData() {
		RLock rlock = redissonClient.getLock(MallProdureVo.class.getName());
		try {
	        if (rlock.tryLock() && this.indexExists()) {
				this.deleteAll();
				log.info("全量同步数据库数据：清空数据");
				
				List<CompletableFuture<Void>> futures=new ArrayList<CompletableFuture<Void>>();
				long start = System.currentTimeMillis();
				
				//重新加载或者没有key的，都重新全量加载数据
				if(isInitLoad || !redisTemplate.hasKey(loadPageKey)) {
					if(redisTemplate.hasKey(loadPageKey)) {
		     			redisTemplate.delete(loadPageKey);
		     		}
					//查出总页数
					Page<MallProdure> pageMax=mallProdureService.page(new Page<MallProdure>(1,pageSize).addOrder(OrderItem.asc("fd_mpro_id")));
					if(pageMax==null || pageMax.getPages()<0) {
						log.info("全量同步数据库数据：无数据同步");
						return;
					}
					IntStream.rangeClosed(1, (int)(pageMax.getPages()+1)).forEachOrdered(values->{
						redisTemplate.opsForList().rightPush(loadPageKey, values);
					});
				}
				
				log.info("全量同步数据库数据：加载总页码成功,总页码数_",redisTemplate.opsForList().size(loadPageKey));

				//同时启动threadSize数量的线程处理数据
				IntStream.rangeClosed(1,threadSize).forEach(i->{
					//开启异步保存数据ES
					CompletableFuture<Void> future = CompletableFuture.completedFuture(i)
				            .thenAcceptAsync(j -> {
				            	while (true) {
				            		Object pageNum=redisTemplate.opsForList().leftPop(loadPageKey);
				            		try {
					            		if(!Objects.isNull(pageNum)) {
						            		long increment=Long.parseLong(String.valueOf(pageNum));
											log.info("全量同步数据库数据：开始同步数据页码:{}",increment);
											
							            	Page<MallProdure> page=mallProdureService.page(new Page<MallProdure>(increment,pageSize).addOrder(OrderItem.asc("fd_mpro_id")));
							            	if(page!=null && !CollectionUtil.isEmpty(page.getRecords())) {
							            		List<MallProdure> list=page.getRecords();
							            		
							            		this.saveAll(MallProdureConverter.DBToES(list, true));
							            		
							            		log.info("全量同步数据库数据：成功同步数据页码:{}，数据量：{}",increment,page.getRecords().size());
							            	}
					            		}else {
					            			log.error("全量同步数据库数据：线程运行已完成，退出线程，ID：{}",j);
					            			break;
					            		}
				            		} catch (Exception e) {
				            			redisTemplate.opsForList().rightPush(loadPageKey, pageNum);
				            			log.error("全量同步数据库数据：重新缓存页码：{}，【异常...】",pageNum);
				            			//e.printStackTrace();
									}
				            	}
				            },threadPoolExecutor);
					futures.add(future);
				});
				CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).get();
				long end = System.currentTimeMillis();
				log.info("全量同步数据库数据：全部成功，{}秒 ",(end-start)/1000);
				
				if(redisTemplate.hasKey(loadPageKey)) {
	     			redisTemplate.delete(loadPageKey);
	     		}
				log.info("全量同步数据库数据：全部成功，解除Redis_KEY成功");
	        }
		} catch (Exception e) {
			log.error("全量同步数据库数据：同步数据异常:{}",e.getMessage());
			//e.printStackTrace();
		} finally {
     		 if(rlock.isHeldByCurrentThread()){
     			rlock.unlock();
     		 }
     	}
	}
	
	

}


