package com.dg.mall.cache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication	
public class MallCacheServiceApplication {

    public static void main(String[] args) {
    	 System.setProperty("es.set.netty.runtime.available.processors", "false");
    	 SpringApplication.run(MallCacheServiceApplication.class, args);
    }

}
