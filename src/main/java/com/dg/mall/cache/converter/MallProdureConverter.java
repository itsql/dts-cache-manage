package com.dg.mall.cache.converter;

import java.util.ArrayList;
import java.util.List;

import com.dg.mall.cache.entity.MallProdure;
import com.dg.mall.cache.modules.elasticsearch.model.MallProdureVo;

import cn.hutool.core.bean.BeanUtil;


/**
 * 商品数据相关的转换器，针对数据库与ES字段名不一的情况需要转换
 * @param mallProdures
 * @param ignoreCase
 * @return
 */
public class MallProdureConverter {
	

	public static List<MallProdureVo> DBToES(List<MallProdure> mallProdures,boolean ignoreCase) {
		List<MallProdureVo> mallProdureVos=new ArrayList<MallProdureVo>();
		mallProdures.parallelStream().filter(mallProdure -> null!=mallProdure).forEach(mallProdure -> {
			mallProdureVos.add(DBToES(mallProdure,ignoreCase));
		});
		return mallProdureVos;
	}
	
	public static MallProdureVo DBToES(MallProdure mallProdure,boolean ignoreCase) {
		MallProdureVo mallProdureVo=new MallProdureVo();
		BeanUtil.copyProperties(mallProdure, mallProdureVo, ignoreCase);
		
		/**
		 * 自定义字段
		 */
		
		
		
		return mallProdureVo;
	}	
	
	

}
