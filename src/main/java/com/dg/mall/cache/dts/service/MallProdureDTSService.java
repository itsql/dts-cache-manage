package com.dg.mall.cache.dts.service;

import java.util.concurrent.ThreadPoolExecutor;

import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.dg.mall.cache.dao.MallProdureDao;
import com.dg.mall.cache.entity.MallProdure;
import com.dg.mall.cache.modules.elasticsearch.service.MallProdureESService;
import com.dg.mall.cache.service.MallProdureService;


/**
 * 更新商城商品缓存统一管理服务
 *
 */
@Service
public class MallProdureDTSService extends ClusterDTSService<MallProdureDao,MallProdure> {
	
	@Autowired
	private MallProdureESService mallProdureESService;
	
	@Autowired	
	protected RedissonClient redissonClient;
	
	@Autowired
	protected RedisTemplate<String, ?> redisTemplate;
	
	@Autowired	
	protected ThreadPoolExecutor threadPoolExecutor;
	
	@Autowired	
	protected MallProdureService mallProdureService;
	

	
	@Override
	public boolean insertDTSMessage(MallProdure message) {
		
		
		return true;
	}

	@Override
	public boolean updateDTSMessage(MallProdure messageOld, MallProdure messageNew) {
	
		return true; 
	}
	
	

	@Override
	public boolean deleteDTSMessage(MallProdure message) {
		mallProdureESService.deleteById(message.getFdMproId());
		return true;
	}


	
	
	
}
