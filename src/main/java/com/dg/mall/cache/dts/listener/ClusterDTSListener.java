package com.dg.mall.cache.dts.listener;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.dts.subscribe.clients.common.RecordListener;
import com.aliyun.dts.subscribe.clients.record.DefaultUserRecord;
import com.aliyun.dts.subscribe.clients.record.OperationType;
import com.aliyun.dts.subscribe.clients.record.RowImage;
import com.aliyun.dts.subscribe.clients.record.value.Value;
import com.dg.mall.cache.common.utils.Exceptions;
import com.dg.mall.cache.config.MallCacheDTSConfig;
import com.dg.mall.cache.dts.service.ClusterDTSService;
import com.dg.mall.cache.modules.elasticsearch.model.MessageDTSLogVo;
import com.dg.mall.cache.modules.elasticsearch.service.MessageDTSLogESService;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Component
public class ClusterDTSListener implements RecordListener {
	
	@Autowired
	private ApplicationContext  applicationContext;
	
	@Autowired
 	private MessageDTSLogESService messageDTSLogESService;
	
	/**
	 * 消费DTS数据
	 */
	@Override
	public void consume(DefaultUserRecord record) {
		
        //消息类型，目前只订阅了增删改的类型，其它类型直接过滤。
        OperationType operationType = record.getOperationType();
        if(!operationType.equals(OperationType.INSERT) && !operationType.equals(OperationType.UPDATE) && !operationType.equals(OperationType.DELETE)) {
        	record.commit("");
        	return;
        }
        
		//数据表名称
		String tableName=record.getSchema().getTableName().get();
		//时间位点
		long timestamp=record.getSourceTimestamp();
		
		//只有订阅的数据库表才会进入处理，否则直接返回确认消费。
		if(MallCacheDTSConfig.subscribeMap.containsKey(tableName)) {
			//消费ID
			long id=record.getId();
			//数据库名称
			String databaseName=record.getSchema().getDatabaseName().get();
			//位点
			int partition=record.getTopicPartition().partition();
			//队列名称
			String topic=record.getTopicPartition().topic();
	
			//只有订阅的表数据才会记录
			MessageDTSLogVo messageLog=new MessageDTSLogVo();
			messageLog.setId(id);
			messageLog.setDatabaseName(databaseName);
			messageLog.setTableName(tableName);
			messageLog.setTimestamp(timestamp);
			messageLog.setPartition(partition);
			messageLog.setTopic(topic);
			messageLog.setCreateTime(new Date());
			
		    try {
				JSONObject beforeMessage=convertFields(record.getBeforeImage());//前数据
				JSONObject message=convertFields(record.getAfterImage());//后数据
				
				messageLog.setMessageType(operationType.name());
				messageLog.setMessageOld(beforeMessage!=null ? beforeMessage.toJSONString() : "");
				messageLog.setMessage(message!=null ? message.toJSONString() : "");

				Class<?> serviceClazz=MallCacheDTSConfig.subscribeMap.get(tableName);
				ClusterDTSService<?, ?> service=(ClusterDTSService<?, ?>)applicationContext.getBean(serviceClazz);

				if(operationType.equals(OperationType.INSERT)){// 数据新增
					
					service.insertDTSMessage(message);
					
				}else if(operationType.equals(OperationType.UPDATE)) {// 数据更新
					
					service.updateDTSMessage(beforeMessage, message);
					
				}else if(operationType.equals(OperationType.DELETE)) {// 数据删除
					
					service.deleteDTSMessage(beforeMessage);
					
				}else if(operationType.equals(OperationType.DDL)) {// DDL 保留，暂不需要处理
					
					
				}else if(operationType.equals(OperationType.HEARTBEAT)) {//HEARTBEAT  保留，暂不需要处理
				
					
				}
				messageLog.setSuccessTime(new Date());
				log.info("DTS订阅消费数据成功，ID：{} ",id);
			}catch (Exception e) {
				messageLog.setException(Exceptions.getStackTraceAsString(e));
				log.error("DTS订阅消费数据失败，ID：{}",id);
				
				//后期添加报警通知...
			}finally {
				record.commit("");  
				messageDTSLogESService.saveAsync(messageLog);
			}
		}else {
			//直接确认消费。
			record.commit("");  
		}
	}
	
	/**
	 * 将消息组成JSON格式输出
	 * @param rowImage
	 * @return
	 */
	public JSONObject convertFields(RowImage rowImage) {
		JSONObject ret = new JSONObject();
		if(rowImage!=null) {	
			@SuppressWarnings("rawtypes")
			Map<String, Value> map=rowImage.toMap(null, null);
			Set<String> keys=map.keySet();
			keys.parallelStream().forEach(key->{
				ret.put(lineToHump(key), map.get(key)!=null ? map.get(key).toString() : null);
			});
		}
		return ret;
	}
	
	/** 下划线转驼峰 */
	private static Pattern linePattern = Pattern.compile("_(\\w)");
	public static String lineToHump(String str) {
		str = str.toLowerCase(); 
		Matcher matcher = linePattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}
 
	
	
}
