package com.dg.mall.cache.dts.store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import com.aliyun.dts.subscribe.clients.metastore.AbstractUserMetaStore;

@Component
public class ClusterDTSMetaStore  extends AbstractUserMetaStore {
	
	@Autowired
	private StringRedisTemplate stringRedisTemplate;

    @Override
    protected void saveData(String groupID, String toStoreJson) {
    	stringRedisTemplate.opsForValue().set(groupID, toStoreJson);
    }

    @Override
    protected String getData(String groupID) {
    	if(stringRedisTemplate.hasKey(groupID)) {
    		return stringRedisTemplate.opsForValue().get(groupID);
    	}
        return null;
    }
    
}