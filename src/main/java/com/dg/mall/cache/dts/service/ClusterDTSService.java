package com.dg.mall.cache.dts.service;

import java.util.HashSet;
import java.util.Set;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 
 * DTS的信息消费接口，只要实现了这个类
 * 重写insertDTSMessage，updateDTSMessage，deleteDTSMessage三个方法
 * 就可以自动消费DTS的数据
 * @author wuwen
 * 2021年7月9日14:52:4
 * @param <M>
 *
 */
public abstract class ClusterDTSService<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> {
	/**
	 * 得到T的class
	 * @return
	 */
	public Class<?> getEntityClass() {return this.entityClass;}
	
	/**
	 * 得到Mapper的class
	 * @return
	 */
	public Class<?> getMapperClass() {return this.mapperClass;}
	
	/**
	 * 得到实体类的数据库表名
	 * @return
	 */
	public String getTableName() {return TableInfoHelper.getTableInfo(this.entityClass).getTableName();}
	
	
	/**
	 * JSON转为对应的实体类
	 * @param message
	 * @return
	 */
	public T convertEntity(JSONObject message) {return message.toJavaObject(this.currentModelClass());}
	
	/**
	 * 得到数据库发生值改变的字段。
	 * 返回的是数据库的实体名称列表。
	 * @param message
	 * @return
	 */
	public Set<String> changeFields(T objectOld,T objectNew) {
		JSONObject messageOld =(JSONObject) JSONObject.toJSON(objectOld);
		JSONObject messageNew =(JSONObject) JSONObject.toJSON(objectNew);
		Set<String> keys=new HashSet<String>();
		messageNew.keySet().parallelStream().forEach(key->{
			if(!messageOld.containsKey(key) || !messageOld.get(key).equals(messageNew.get(key))) {
				keys.add(key);
			}
		});
		return keys;
	}
	
	/**
	 * 针对JSON转换实体方法
	 */
	public boolean insertDTSMessage(JSONObject message) {return this.insertDTSMessage(convertEntity(message));}
	public boolean updateDTSMessage(JSONObject messageOld,JSONObject messageNew) {return this.updateDTSMessage(convertEntity(messageOld),convertEntity(messageOld));}
	public boolean deleteDTSMessage(JSONObject message) {return this.deleteDTSMessage(convertEntity(message));}
	
	
	/**
	 * 订阅DTS的insert数据
	 * @param message
	 * @return
	 */
	public abstract boolean insertDTSMessage(T message);
	
	/**
	 * 订阅DTS的update数据
	 * @param message
	 * @return
	 */
	public abstract boolean updateDTSMessage(T messageOld,T messageNew);
	
	/**
	 * 订阅DTS的delete数据
	 * @param message
	 * @return
	 */
	public abstract boolean deleteDTSMessage(T message);

	
}
