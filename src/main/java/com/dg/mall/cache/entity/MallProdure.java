package com.dg.mall.cache.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

@Data
@TableName("mall_produre")
public class MallProdure implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@TableId
    private Integer fdMproId;      
	
    private String fdMproNo;        
    
    private String fdMproName;        
    
    private String fdMproSubhead;		
    
    private String fdMproGuige1;       
    
    private String fdMproGuige2;      
    
    private String fdMproGuige3;       
    
    private String fdMproTese;       




}
