package com.dg.mall.cache.controller.elasticsearch;

import java.util.Map;

import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dg.mall.cache.common.utils.R;
import com.dg.mall.cache.dts.service.MallProdureDTSService;
import com.dg.mall.cache.modules.elasticsearch.service.MessageDTSLogESService;
import com.dg.mall.cache.service.MallProdureService;



/**
 * es的缓存管理
 * @author wuwen
 * @email 
 * @date 2021-05-22 19:49:53
 */
@RestController
@RequestMapping("cache/es")
public class ElasticSearchController {
	
	@Autowired
	private MallProdureService mallProdureService;
	
	@Autowired
	private MallProdureDTSService mallProdureDTSService;
	
	@Autowired
	protected RedisTemplate<String, ?> redisTemplate;
	
	@Autowired
	protected RedissonClient redissonClient;
	
	
    @Autowired
	private ApplicationContext  applicationContext;
    
	@Autowired
 	private MessageDTSLogESService messageDTSLogESService;


    @RequestMapping(value = "/{key}")
    public R key(@PathVariable("key") String key) {
        return R.ok().setData(null);
    }
    
    
    @RequestMapping(value = "/get")
    public R key() throws Exception {
    	return R.ok();
    }
    

    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params){
    	
        return R.ok().setData(null);
    }


    @RequestMapping("/save")
    public R save(@RequestBody  Object obj){
		
        return R.ok();
    }
    

    @RequestMapping("/delete")
    public R delete(@RequestBody String[] keys){
		
        return R.ok();
    }

    
    @RequestMapping("/update")
    public R update(@RequestBody Object obj){
    	
        return R.ok();
    }

	
	

}
