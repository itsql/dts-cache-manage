package com.dg.mall.cache.controller.redis;

import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dg.mall.cache.common.BaseController;
import com.dg.mall.cache.common.utils.R;



/**
 * redis的缓存管理
 * @author wuwen
 * @email 
 * @date 2021-05-22 19:49:53
 */
@RestController
@RequestMapping("cache/redis")
public class RedisController extends BaseController {
	


    @RequestMapping(value = "/{key}")
    public R key(@PathVariable("key") String key) {
    	
        return R.ok().setData(null);
    }
    
   
    @RequestMapping(value = "/get")
    public R key(@RequestBody String[] keys) {
    	
        return R.ok().setData(null);
    }
    

    @RequestMapping("/list")
    public R list(@RequestBody Map<String, Object> params){
    	
        return R.ok().setData(null);
    }


    @RequestMapping("/save")
    public R save(@RequestBody  Object obj){
		
        return R.ok();
    }
  
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] keys){
		
        return R.ok();
    }

  
    @RequestMapping("/update")
    public R update(@RequestBody Object obj){
    	
        return R.ok();
    }

   
}
