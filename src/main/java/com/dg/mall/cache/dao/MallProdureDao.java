package com.dg.mall.cache.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dg.mall.cache.entity.MallProdure;


@Mapper
public interface MallProdureDao extends BaseMapper<MallProdure> {

	
	
}
