package com.dg.mall.cache.service;

import java.util.concurrent.ThreadPoolExecutor;

import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dg.mall.cache.dao.MallProdureDao;
import com.dg.mall.cache.entity.MallProdure;

@Service
public class MallProdureService  extends ServiceImpl<MallProdureDao,MallProdure> {
	
	@Autowired	
	protected RedissonClient redissonClient;
	
	@Autowired
	protected RedisTemplate<String, ?> redisTemplate;
	
	@Autowired	
	protected ThreadPoolExecutor threadPoolExecutor;


	
}

   

