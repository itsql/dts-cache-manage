package com.dg.mall.cache.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApiAuthenticationFilter implements Filter{


	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		log.info("过滤器init...");
			
		
	}

	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		log.info("过滤器doFilter...");
		//HttpServletRequest request = (HttpServletRequest) servletrequest;
		//HttpServletResponse response = (HttpServletResponse) servletresponse;
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		log.info("过滤器destroy...");

	}



}
